data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module gral_server_sg {
  source             = "../aws/vpc/aws_security_group"
  tags               = var.tags
  name               = "Gral-server-sg"
  description        = "Gral-server-sg"
  vpc_id             = var.vpc_id
  cidr_ingress_rules = var.cidr_ingress_rules
}

resource aws_instance gral_server {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = module.gral_server_sg.id
  iam_instance_profile   = var.instance_profile
  subnet_id              = var.subnet
  tags                   = { "Name" : "gralServer" }
}

#### Bastion Server ###############
module gral_server {
  source             = "./gral_server"
  key_name           = "bastionKey"
  instance_profile   = "bastionRole"
  tags               = data.null_data_source.extra_tags.inputs
  cidr_ingress_rules = ["30.0.0.0/8"]
  subnet             = module.networking.application_subnets[0]
  vpc_id             = module.networking.vpc_id
}

# resource aws_eip bastion_eip {
#     instance = aws_instance.bastion_server.id
#     vpc = true
# }