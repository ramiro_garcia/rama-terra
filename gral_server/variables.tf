variable key_name {}

variable instance_profile {}

variable cidr_ingress_rules {}

variable subnet {}

variable vpc_id {}

variable tags {}