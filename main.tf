data aws_region current {}

data aws_caller_identity current {}

#Requiring a minimum Terraform version to execute a configuration
terraform {
  required_version = "~> 0.12"

  backend "s3" {
    bucket  = "aivo-terraform-beta"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    encrypt = "true"
    profile = "aivo-dev"
  }
}

#The provider variables for used the services
provider "aws" {
  version = "~> 2.43"
  region  = "us-east-1"
  profile = "aivo-dev"
}

data null_data_source extra_tags {
  inputs = {
    Environment  = var.environment
    Region       = data.aws_region.current.name
    Owner        = var.user
    Team         = var.team
    Architecture = "2"
    Terraform    = "True"
  }
}
