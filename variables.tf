variable environment {}

variable application {}

variable vpc_cidr_block {}

variable user {}

variable team {}

#variable key_name {}

variable availability_zones {
  type = list
}

variable subnet_size {}

variable subnet_assignment {
  type = map

  default = {
    "public"      = "1,2"
    "application" = "3,4"
    "persistence" = "5,6"
  }
}

variable team_subdomain {}

variable environments {
  type = list
}