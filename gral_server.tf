#### Bastion Server ###############
module gral_server {
  source             = "/gral_server"
  key_name           = "bastionKey"
  instance_profile   = "AmazonSSMRoleForInstancesQuickSetup"
  tags               =  name.gral_server
  cidr_ingress_rules = ["30.0.0.0/8"]
  subnet             = subnet-71a8d138
  vpc_id             = vpc-e460af82
}
